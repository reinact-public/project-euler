﻿#include <iostream>
#include <map>


int main()
{
	int max_length = 0;
	int max_value = 2;
	for (int n = 3; n < 1000; ++n)
	{
		int carry = 10;
		std::map<int, int> cache;
		for (int pos = 1; ; ++pos)
		{
			if (carry % n == 0)
			{
				break;
			}
			const decltype(cache)::iterator it = cache.find(carry % n);
			if (it != cache.end())
			{
				if (max_length < pos - it->second)
				{
					max_length = pos - it->second;
					max_value = n;
				}
				break;
			}
			cache[carry % n] = pos;
			carry = (carry % n) * 10;
		}
	}
	std::cout << max_value << std::endl;

	return 0;
}
