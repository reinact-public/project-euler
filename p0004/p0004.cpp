﻿#include <iostream>
#include <algorithm>


static int reverse(int d)
{
	int rval = 0;

	for (; d != 0; d /= 10)
	{
		rval = rval * 10 + (d % 10);
	}
	return rval;
}


int main()
{
	int rval = 0;

	for (int i = 100; i <= 999; ++i)
	{
		for (int j = i; j <= 999; ++j)
		{
			int d = i * j;
			if (d != reverse(d))
			{
				continue;
			}
			rval = std::max(rval, d);
		}
	}
	std::cout << rval << std::endl;

	return 0;
}
