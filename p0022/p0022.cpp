﻿#include <iostream>
#include <algorithm>
#include <string>
#include <vector>


static int score(const std::string &s)
{
	int rvalue = 0;
	for (std::string::const_iterator it = s.cbegin(); it != s.cend(); ++it)
	{
		rvalue += *it - 'A' + 1;
	}
	return rvalue;
}


int main()
{
	std::vector<std::string> names{
#include "names.h"
	};

	std::sort(names.begin(), names.end());

	int total = 0;
	for (int i = 0; i < names.size(); ++i)
	{
		total += score(names[i]) * (i + 1);
	}
	std::cout << total << std::endl;

	return 0;
}
