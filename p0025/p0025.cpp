﻿#include <iostream>
#include <pyint.h>


int main()
{
	pyint p("0"), c("1");
	
	for (size_t i = 1; ; ++i)
	{
		if (c.str().size() >= 1000)
		{
			std::cout << i << std::endl;
			break;
		}
		pyint t = c;
		c += p;
		p = t;
	}

	return 0;
}
