﻿#include <iostream>
#include <vector>


int main()
{
	std::vector<std::vector<int>> d;
	for (int i = 0; i < 20; ++i)
	{
		std::vector<int> line;
		for (int j = 0; j < 20; ++j)
		{
			int d;
			std::cin >> d;
			line.push_back(d);
		}
		d.push_back(line);
	}

	int max_prod = 0;
	for (int i = 0; i < 20; ++i)
	{
		for (int j = 0; j < 20; ++j)
		{
			int prod;

			if (i + 4 <= 20)
			{
				prod = 1;
				for (int k = 0; k < 4; ++k)
				{
					prod *= d[i + k][j];
				}
				max_prod = std::max(max_prod, prod);
			}

			if (j + 4 <= 20)
			{
				prod = 1;
				for (int k = 0; k < 4; ++k)
				{
					prod *= d[i][j + k];
				}
				max_prod = std::max(max_prod, prod);
			}

			if (i + 4 <= 20 && j + 4 <= 20)
			{
				prod = 1;
				for (int k = 0; k < 4; ++k)
				{
					prod *= d[i + k][j + k];
				}
				max_prod = std::max(max_prod, prod);

				prod = 1;
				for (int k = 0; k < 4; ++k)
				{
					prod *= d[i + k][j + 3 - k];
				}
				max_prod = std::max(max_prod, prod);
			}
		}
	}
	std::cout << max_prod << std::endl;

	return 0;
}
