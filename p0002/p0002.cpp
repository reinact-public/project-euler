﻿#include <iostream>

int main()
{
	int sum = 0;

	for (int p = 1, c = 1; c < 4000000; )
	{
		if (c % 2 == 0)
		{
			sum += c;
		}

		int t = p;
		p = c;
		c += t;
	}
	std::cout << sum << std::endl;

	return 0;
}
