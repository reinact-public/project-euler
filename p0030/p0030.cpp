﻿#include <iostream>

static constexpr int UPPER_BOUND = 9 * 9 * 9 * 9 * 9 * 6;


int main()
{
	int total = 0;
	for (int i = 2; i <= UPPER_BOUND; ++i)
	{
		int sum = 0;
		for (int j = i; j > 0; j /= 10)
		{
			sum += (j % 10) * (j % 10) * (j % 10) * (j % 10) * (j % 10);
		}
		if (i == sum)
		{
			total += i;
		}
	}
	std::cout << total << std::endl;

	return 0;
}
