﻿#include <iostream>
#include <string>
#include <cctype>

const std::string LEVEL1_TABLE[] = {
	"zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten",
	"eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"
};
const std::string LEVEL2_TABLE[] = {
	"", "", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety"
};
const std::string LEVEL3_TABLE[] = {
	"and", "hundred", "thousand"
};


std::string secure_itoa(int number)
{
	if (number < 0 || number > 1000)
	{
		return "";
	}
	if (number == 0)
	{
		return LEVEL1_TABLE[0];
	}
	if (number == 1000)
	{
		return LEVEL1_TABLE[1] + " " + LEVEL3_TABLE[2];
	}

	std::string rvalue = "";
	if (number >= 100)
	{
		rvalue += LEVEL1_TABLE[(number / 100) % 10];
		rvalue += " " + LEVEL3_TABLE[1];
		if (number % 100 == 0)
		{
			return rvalue;
		}
		rvalue += " " + LEVEL3_TABLE[0] + " ";
	}

	const int lower100 = number % 100;
	if (lower100 < 20)
	{
		rvalue += LEVEL1_TABLE[lower100];
	}
	else
	{
		rvalue += LEVEL2_TABLE[lower100 / 10];
		if (lower100 % 10 > 0)
		{
			rvalue += "-" + LEVEL1_TABLE[lower100 % 10];
		}
	}

	return rvalue;
}


static size_t secure_strlen(const std::string &s)
{
	size_t len = 0;
	for (std::string::const_iterator it = s.cbegin(); it != s.cend(); ++it)
	{
		len += !!(isalpha(*it));
	}
	return len;
}


int main()
{
	size_t total = 0;

	for (int i = 1; i <= 1000; ++i)
	{
		const std::string s = secure_itoa(i);
		const size_t l = secure_strlen(s);
		std::cout << i << ": " << s << " (" << l << ")" << std::endl;
		total += l;
	}
	std::cout << std::endl << total << std::endl;

	return 0;
}
