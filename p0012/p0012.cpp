﻿#include <iostream>
#include <vector>


template<typename T>
static constexpr std::vector<std::pair<T, size_t>> pfactor(T d)
{
    std::vector<std::pair<T, size_t>> rval;
    for (T n = 2; n * n <= d; ++n)
    {
        size_t count;
        for (count = 0; d % n == 0; ++count)
        {
            d /= n;
        }
        if (count > 0)
        {
            rval.push_back(std::make_pair(n, count));
        }
    }
    if (d > 1)
    {
        rval.push_back(std::make_pair(d, 1));
    }
    return rval;
}



int main()
{
    int d = 0;
    for (int i = 1; ; ++i)
    {
        d += i;

        int total = 1;
        for (std::pair<int, size_t> p : pfactor(d))
        {
            total *= p.second + 1;
        }
        if (total > 500)
        {
            std::cout << d << std::endl;
            break;
        }
    }

    return 0;
}
