﻿#include <iostream>
#include <vector>


int main()
{
	std::vector<size_t> abundants;
	for (size_t i = 2; i < 28123; ++i)
	{
		int sum = 1;
		for (size_t j = 2; j * j <= i; ++j)
		{
			if (i % j != 0)
			{
				continue;
			}
			sum += j;
			if (j * j != i)
			{
				sum += i / j;
			}
		}
		if (i < sum)
		{
			abundants.push_back(i);
		}
	}

	std::vector<bool> flags(28123, true);
	for (int i = 0; i < abundants.size(); ++i)
	{
		for (int j = i; j < abundants.size() && abundants[i] + abundants[j] < flags.size(); ++j)
		{
			flags[abundants[i] + abundants[j]] = false;
		}
	}

	size_t total = 0;
	for (size_t i = 1; i < flags.size(); ++i)
	{
		if (flags[i])
		{
			total += i;
		}
	}
	std::cout << total << std::endl;

	return 0;
}
