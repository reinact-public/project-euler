﻿#include <iostream>
#include <vector>
#include <algorithm>


int main()
{
	std::vector<int> arr{ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };

	for (int i = 1; i < 1000000; ++i)
	{
		std::next_permutation(arr.begin(), arr.end());
	}
	for (const int d : arr)
	{
		std::cout << d;
	}
	std::cout << std::endl;

	return 0;
}
