﻿#include <iostream>
#include <set>
#include "pyint.h"


int main()
{
	std::set<pyint> stack;

	for (int i = 2; i <= 100; ++i)
	{
		pyint a(std::to_string(i));
		pyint b = a * a;
		for (int j = 2; j <= 100; ++j)
		{
			stack.insert(b);
			b *= a;
		}
	}
	std::cout << stack.size() << std::endl;

	return 0;
}
