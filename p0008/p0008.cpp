﻿#include <iostream>
#include <vector>
#include <algorithm>
#include <cctype>


int main()
{
	std::vector<int> d;
	for (; ; )
	{
		int ch = std::cin.get();
		if (ch == EOF)
		{
			break;
		}
		if (isdigit(ch))
		{
			d.push_back(ch - '0');
		}
	}

	uint64_t max_prod = 0;
	for (int i = 0; i + 13 < d.size(); ++i)
	{
		uint64_t prod = d[i];
		for (int j = 1; j < 13; ++j)
		{
			prod *= d[i + j];
		}
		max_prod = std::max(max_prod, prod);
	}
	std::cout << max_prod << std::endl;

	return 0;
}
