﻿#include <iostream>
#include <algorithm>
#include <vector>
#include "pymath.h"


int main()
{
	const std::vector<size_t> primes = pymath::primes(2001001);
	int max_length = 0;
	int max_value = 0;
	for (const int b : primes)
	{
		if (b > 1000)
		{
			break;
		}
		for (int a = 1 - b; a < 1000; ++a)
		{
			for (int n = 1; ; ++n)
			{
				if (!std::binary_search(primes.cbegin(), primes.cend(), n * n + a * n + b))
				{
					if (n > max_length)
					{
						max_length = n;
						max_value = a * b;
					}
					break;
				}
			}
		}
	}
	std::cout << max_value << std::endl;

	return 0;
}
