#include "pch.h"
#include <iostream>
#include <iomanip>
#include <sstream>
#include <algorithm>
#include <cctype>
#include "libcommon/pyint.h"

static constexpr int64_t M = 100000000;
static constexpr int64_t M_WIDTH = 8;


pyint::pyint() : pyint::pyint("0") { }
pyint::pyint(const std::string& s)
{
	for (std::remove_reference<decltype(s)>::type::const_reverse_iterator it = s.crbegin(); it != s.crend();)
	{
		int q[M_WIDTH];
		int p = 0;
		for (int i = 0; it != s.crend() && i < M_WIDTH; ++it)
		{
			if (isdigit(*it))
			{
				q[p++] = *it - '0';
			}
		}

		int64_t digit = 0;
		for (--p; p >= 0; --p)
		{
			digit = digit * 10 + q[p];
		}
		this->d.push_back(digit);
	}
}


pyint pyint::operator+(const pyint& o) const
{
	return pyint(*this) += o;
}
pyint& pyint::operator+=(const pyint& o)
{
	if (this->d.size() < o.d.size())
	{
		this->d.resize(o.d.size(), 0);
	}

	decltype(this->d)::iterator it1;
	decltype(o.d)::const_iterator it2;
	for (it1 = this->d.begin(), it2 = o.d.begin(); it2 != o.d.end(); ++it1, ++it2)
	{
		*it1 += *it2;
	}
	
	int64_t carry = 0;
	for (it1 = this->d.begin(); it1 != this->d.end(); ++it1)
	{
		*it1 += carry;
		carry = *it1 / M;
		*it1 %= M;
	}
	if (carry != 0)
	{
		this->d.push_back(carry);
	}

	return *this;
}

pyint pyint::operator*(const pyint& o) const
{
	pyint sum;

	for (int i = 0; i < o.d.size(); ++i)
	{
		pyint d;
		d.d.resize(i, 0);
		int64_t carry = 0;
		for (decltype(this->d)::const_iterator it = this->d.cbegin(); it != this->d.cend() || carry != 0; )
		{
			if (it != this->d.cend())
			{
				carry += *it++ * o.d[i];
			}
			d.d.push_back(carry % M);
			carry /= M;
		}
		sum += d;
	}

	return sum;
}
pyint& pyint::operator*=(const pyint& o)
{
	return *this = *this * o;
}


bool pyint::operator<(const pyint& o) const
{
	if (this->d.size() != o.d.size())
	{
		return this->d.size() < o.d.size();
	}
	decltype(this->d)::const_reverse_iterator it1;
	decltype(o.d)::const_reverse_iterator it2;
	for (it1 = this->d.crbegin(), it2 = o.d.crbegin(); it1 != this->d.crend(); ++it1, ++it2)
	{
		if (*it1 != *it2)
		{
			return *it1 < *it2;
		}
	}
	return false;
}
bool pyint::operator>(const pyint& o) const
{
	return o < *this;
}
bool pyint::operator<=(const pyint& o) const
{
	return !(o < *this);
}
bool pyint::operator>=(const pyint& o) const
{
	return !(*this < o);
}


bool pyint::operator==(const pyint& o) const
{
	if (this->d.size() != o.d.size())
	{
		return this->d.size() < o.d.size();
	}
	decltype(this->d)::const_reverse_iterator it1;
	decltype(o.d)::const_reverse_iterator it2;
	for (it1 = this->d.crbegin(), it2 = o.d.crbegin(); it1 != this->d.crend(); ++it1, ++it2)
	{
		if (*it1 != *it2)
		{
			return false;
		}
	}
	return true;
}
bool pyint::operator!=(const pyint& o) const
{
	return !(*this == o);
}


std::string pyint::str() const
{
	std::ostringstream sb;

	for (decltype(this->d)::const_reverse_iterator it = this->d.crbegin(); it != this->d.crend(); ++it)
	{
		if (it == this->d.crbegin())
		{
			sb << *it;
		}
		else
		{
			sb << std::setfill('0') << std::right << std::setw(M_WIDTH) << *it;
		}
	}

	return sb.str();
}


std::ostream& operator<<(std::ostream& s, const pyint& o)
{
	std::cout << o.str();
	return s;
}
