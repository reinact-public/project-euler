#pragma once
#include <iostream>
#include <vector>
#include <string>
#include <cstdint>


class pyint
{
public:
	pyint();
	pyint(const std::string&);
	pyint operator+(const pyint&) const;
	pyint& operator+=(const pyint&);
	pyint operator*(const pyint&) const;
	pyint& operator*=(const pyint&);
	bool operator<(const pyint&) const;
	bool operator>(const pyint&) const;
	bool operator<=(const pyint&) const;
	bool operator>=(const pyint&) const;
	bool operator==(const pyint&) const;
	bool operator!=(const pyint&) const;
	std::string str() const;
private:
	std::vector<int64_t> d;
};


std::ostream& operator<<(std::ostream&, const pyint&);
