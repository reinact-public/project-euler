#pragma once
#include <vector>


namespace pymath
{
	extern std::vector<size_t> primes(size_t);
}
