#include "pch.h"
#include <algorithm>
#include <memory>
#include "libcommon/pymath.h"


namespace pymath
{
	std::vector<size_t> primes(const size_t upper_bound)
	{
		const std::unique_ptr<uint64_t[]> is_prime = std::make_unique<uint64_t[]>(upper_bound / 64 + 1);
		std::vector<size_t> rvalue;
		for (size_t i = 2; i * i <= upper_bound; ++i)
		{
			if (!(is_prime[i / 64] & (uint64_t(1) << (i % 64))))
			{
				rvalue.push_back(i);
				for (size_t j = i * i; j <= upper_bound; j += i)
				{
					is_prime[j / 64] |= uint64_t(1) << (j % 64);
				}
			}
		}
		return rvalue;
	}
}
