﻿#include <iostream>
#include <vector>
#include <stack>
#include <algorithm>


int main()
{
	std::vector<int> cache(1000000, -1);
	cache[0] = cache[1] = 0;

	std::stack<uint64_t> lis;
	int max_start = 1;
	int max_depth = 0;
	for (int i = 2; i < 1000000; ++i)
	{
		int depth;
		for (uint64_t t = i; ; )
		{
			if (t < cache.size() && cache[t] >= 0)
			{
				depth = cache[t];
				break;
			}
			lis.push(t);
			if (t % 2 == 0)
			{
				t /= 2;
			}
			else
			{
				t = t * 3 + 1;
			}
		}
		while (!lis.empty())
		{
			depth += 1;
			if (lis.top() < cache.size())
			{
				cache[lis.top()] = depth;
			}
			lis.pop();
		}
		if (depth > max_depth)
		{
			max_start = i;
			max_depth = depth;
		}
	}
	std::cout << max_start << std::endl;

	return 0;
}
