﻿#include <iostream>
#include <algorithm>
#include <vector>


template<typename T>
static constexpr T power(T x, T y)
{
    if (y < 0)
    {
        return 0;
    }
    T r = 1;
    while (y > 0)
    {
        if (y % 2 == 1)
        {
            r *= x;
        }
        y /= 2;
        x *= x;
    }
    return r;
}


template<typename T>
static constexpr std::vector<std::pair<T, size_t>> pfactor(T d)
{
    std::vector<std::pair<T, size_t>> rval;
    for (T n = 2; n * n <= d; ++n)
    {
        size_t count;
        for (count = 0; d % n == 0; ++count)
        {
            d /= n;
        }
        if (count > 0)
        {
            rval.push_back(std::make_pair(n, count));
        }
    }
    if (d > 1)
    {
        rval.push_back(std::make_pair(d, 1));
    }
    return rval;
}


int main()
{
    std::vector<size_t> buckets(21);

    for (int i = 2; i < buckets.size(); ++i)
    {
        for (std::pair<int, size_t> p : pfactor(i))
        {
            buckets[p.first] = std::max(buckets[p.first], p.second);
        }
    }

    size_t result = 1;
    for (size_t i = 2; i < buckets.size(); ++i)
    {
        result *= power(i, buckets[i]);
    }
    std::cout << result << std::endl;

    return 0;
}
