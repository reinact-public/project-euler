﻿#include <iostream>
#include <vector>


static int d(int value)
{
    int sum = 1;
}


int main()
{
    std::vector<int> d(10000);
    for (int i = 2; i < d.size(); ++i)
    {
        d[i] = 1;
        for (int j = 2; j * j <= i; ++j)
        {
            if (i % j != 0)
            {
                continue;
            }
            d[i] += j;
            if (j * j != i)
            {
                d[i] += i / j;
            }
        }
    }

    int sum = 0;
    for (int i = 2; i < d.size(); ++i)
    {
        if (d[i] < d.size() && d[i] != i && d[d[i]] == i)
        {
            sum += i;
        }
    }
    std::cout << sum << std::endl;

    return 0;
}
