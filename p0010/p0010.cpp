﻿#include <iostream>
#include <vector>


static std::vector<size_t> primes(size_t upper_bound)
{
    std::vector<bool> sieve(upper_bound + 1);
    for (size_t i = 2; i * i <= upper_bound; ++i)
    {
        if (sieve[i])
        {
            continue;
        }
        for (size_t j = i * i; j <= upper_bound; j += i)
        {
            sieve[j] = true;
        }
    }

    std::vector<size_t> rvalues;
    for (size_t i = 2; i <= upper_bound; ++i)
    {
        if (!sieve[i])
        {
            rvalues.push_back(i);
        }
    }
    return rvalues;
}


int main()
{
    uint64_t sum = 0;
    for (size_t prime : primes(1999999))
    {
        sum += prime;
    }
    std::cout << sum << std::endl;

    return 0;
}
