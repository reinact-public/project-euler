﻿#include <iostream>

int main()
{
	int sq_sum = 0;
	int sum_sq = 0;

	for (int i = 1; i <= 100; ++i)
	{
		sq_sum += i;
		sum_sq += i * i;
	}
	sq_sum *= sq_sum;

	std::cout << (sq_sum - sum_sq) << std::endl;

	return 0;
}
