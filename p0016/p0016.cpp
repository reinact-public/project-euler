﻿#include <iostream>
#include <vector>


static std::string stradd(const std::string& s1, const std::string& s2)
{
	std::vector<char> d;

	std::string::const_reverse_iterator it1 = s1.crbegin();
	std::string::const_reverse_iterator it2 = s2.crbegin();
	for (int carry = 0; it1 != s1.crend() || it2 != s2.crend() || carry != 0; )
	{
		if (it1 != s1.crend())
		{
			carry += *it1++ - '0';
		}
		if (it2 != s2.crend())
		{
			carry += *it2++ - '0';
		}
		d.push_back('0' + (carry % 10));
		carry /= 10;
	}
	if (d.empty())
	{
		d.push_back('0');
	}

	return std::string(d.crbegin(), d.crend());
}


static std::string strmul(const std::string& s1, const std::string& s2)
{
	std::string sum = "0";
	std::vector<char> d;

	for (int i = 0; i < s2.size(); ++i)
	{
		const int digit = s2[s2.size() - 1 - i] - '0';
		d.clear();
		d.resize(i, '0');
		int carry = 0;
		for (std::string::const_reverse_iterator it = s1.crbegin(); it != s1.crend() || carry != 0; )
		{
			if (it != s1.crend())
			{
				carry += (*it++ - '0') * digit;
			}
			d.push_back('0' + (carry % 10));
			carry /= 10;
		}
		sum = stradd(sum, std::string(d.crbegin(), d.crend()));
	}

	return sum;
}


static std::string strpow(std::string s, size_t p)
{
	std::string prod = "1";
	while (p > 0)
	{
		if (p % 2 == 1)
		{
			prod = strmul(prod, s);
		}
		s = strmul(s, s);
		p /= 2;
	}
	return prod;
}


int main()
{
	const std::string d = strpow("2", 1000);
	int64_t sum = 0;
	for (std::string::const_iterator it = d.cbegin(); it != d.cend(); ++it)
	{
		sum += *it - '0';
	}
	std::cout << sum << std::endl;

	return 0;
}
