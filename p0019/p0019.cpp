﻿#include <iostream>


int main()
{
	int count = 0;
	int day = (1 + 366) % 7;
	for (int year = 1901; year <= 2000; ++year)
	{
		for (int month = 1; month <= 12; ++month)
		{
			count += !!(day == 0);
			switch (month)
			{
			case 1:	case 3:	case 5:	case 7:
			case 8:	case 10: case 12:
				day = (day + 31) % 7;
				break;
			case 2:
				if (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0))
				{
					day = (day + 29) % 7;
				}
				else
				{
					day = (day + 28) % 7;
				}
				break;
			case 4:	case 6:	case 9:	case 11:
				day = (day + 30) % 7;
				break;
			}
		}
	}
	std::cout << count << std::endl;

	return 0;
}
