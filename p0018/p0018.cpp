﻿#include <iostream>
#include <vector>
#include <algorithm>


static constexpr size_t p(size_t row, size_t col)
{
	if (row == 0)
	{
		return col;
	}
	if (row % 2 == 0)
	{
		return (row / 2) * (row + 1) + col;
	}
	else
	{
		return row * ((row + 1) / 2) + col;
	}
}


int main()
{
	std::vector<int> d;
	for (int t; std::cin >> t; )
	{
		d.push_back(t);
	}

	int max_value = 0;
	for (size_t i = 1; p(i, i) < d.size(); ++i)
	{
		max_value = 0;
		for (size_t j = 0; j <= i; ++j)
		{
			int value = 0;
			if (j >= 1)
			{
				value = std::max(value, d[p(i - 1, j - 1)]);
			}
			if (j <= i - 1)
			{
				value = std::max(value, d[p(i - 1, j)]);
			}
			d[p(i, j)] += value;
			max_value = std::max(max_value, d[p(i, j)]);
		}
	}
	std::cout << max_value << std::endl;

	return 0;
}
