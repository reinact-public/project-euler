﻿#include <iostream>
#include <string>
#include <vector>


static std::string stradd(const std::string &s1, const std::string &s2)
{
	std::vector<char> d;
	int carry = 0;

	std::string::const_reverse_iterator it1 = s1.crbegin();
	std::string::const_reverse_iterator it2 = s2.crbegin();
	for (; it1 != s1.crend() || it2 != s2.crend() || carry != 0; )
	{
		if (it1 != s1.crend())
		{
			carry += *it1++ - '0';
		}
		if (it2 != s2.crend())
		{
			carry += *it2++ - '0';
		}
		d.push_back('0' + (carry % 10));
		carry /= 10;
	}
	if (d.empty())
	{
		d.push_back('0');
	}

	return std::string(d.crbegin(), d.crend());
}


int main()
{
	std::string sum = "0";
	for (; ; )
	{
		std::string s;
		std::getline(std::cin, s);
		if (std::cin.bad() || std::cin.eof())
		{
			break;
		}
		sum = stradd(sum, s);
	}
	std::cout << sum.substr(0, 10) << std::endl;
	return 0;
}
