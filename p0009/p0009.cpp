﻿#include <iostream>
#include <algorithm>

int main()
{
	for (int a = 1; a <= 998; ++a)
	{
		for (int b = a; a + b <= 999; ++b)
		{
			const int c = 1000 - a - b;
			if (a * a + b * b == c * c)
			{
				std::cout << a * b * c;
			}
			else if (a * a + b * b > c * c)
			{
				break;
			}
		}
	}

	return 0;
}
