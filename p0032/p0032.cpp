﻿// [x] X * XXX = - 8991
// [o] X * XXXX = 1000 - 89991
// [x] X * XXXXX = 10000 - 
// [x] XX * XX = - 9801
// [o] XX * XXX = 1000 - 98901
// [x] XX * XXXX = 10000 - 
#include <iostream>
#include <array>
#include <set>


static void int2bin(int value, std::array<int, 10> &out)
{
	do {
		out[value % 10] += 1;
		value /= 10;
	} while (value != 0);
}


static bool is_pandigital(const int multiplicand, const int multiplier)
{
	const int product = multiplicand * multiplier;
	std::array<int, 10> digits{ 0 };
	int2bin(multiplicand, digits);
	int2bin(multiplier, digits);
	int2bin(product, digits);
	if (digits[0] != 0) {
		return false;
	}
	for (int i = 1; i <= 9; ++i) {
		if (digits[i] != 1) {
			return false;
		}
	}
	return true;
}


int main()
{
	std::set<int> lis;
	for (int x = 1; x <= 9; ++x) {
		for (int y = 1000; y <= 9999; ++y) {
			if (is_pandigital(x, y)) {
				lis.insert(x * y);
			}
		}
	}
	for (int x = 10; x <= 99; ++x) {
		for (int y = 100; y <= 999; ++y) {
			if (is_pandigital(x, y)) {
				lis.insert(x * y);
			}
		}
	}

	int sum = 0;
	for (const int val : lis)
	{
		sum += val;
	}
	std::cout << sum << std::endl;

	return 0;
}
