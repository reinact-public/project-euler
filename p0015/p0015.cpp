﻿#include <iostream>
#include <vector>
#include <cstdint>


int main()
{
	std::vector<uint64_t> table(21, 1);
	for (size_t i = 1; i <= 20; ++i)
	{
		for (size_t j = 1; j <= 20; ++j)
		{
			table[j] += table[j - 1];
		}
	}
	std::cout << table[20] << std::endl;

	return 0;
}
