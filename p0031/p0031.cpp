﻿#include <iostream>


int main()
{
	int opt[201]{ 1 };

	for (const int c : {1, 2, 5, 10, 20, 50, 100, 200})
	{
		for (int i = 200; i >= 1; --i)
		{
			for (int j = i - c; j >= 0; j -= c)
			{
				opt[i] += opt[j];
			}
		}
	}
	std::cout << opt[200] << std::endl;

	return 0;
}
