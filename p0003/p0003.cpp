﻿#include <cstdint>
#include <iostream>

int main()
{
	uint64_t d = 600851475143;

	for (uint64_t i = 2; i * i <= d; )
	{
		if (d % i == 0)
		{
			d /= i;
		}
		else
		{
			++i;
		}
	}
	std::cout << d << std::endl;

	return 0;
}
